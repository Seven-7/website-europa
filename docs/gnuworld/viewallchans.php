<?
/****************************************************************************************
 *
 * View All Registered Channels! (for 1000 admins only, if you want it otherway, 
 *                               change all 1000 numbers into another level in this file)
 *
 * Coded by:
 *          - Mitko @ Undernet
 *          - |nemo| @ Undernet
 *
 * Put this file (viewallchans.php) in ~gnuworld/website/docs/gnuworld/
 *
 * If you want to see "View all channels" link in the menu, edit left.php and do this:
 *
 * FIND:
 *
   <? if ($admin>=800) { ?>
   <a href="cr_newuser.php" target=right>New User</a><br><br>
   <? } else { echo "<br>"; } ?>
 *
 * and AFTER that ADD:
 *
   <? if ($admin>=800) { ?>
   <a href="viewallchans.php" target=right>View all channels</a><br><br>
   <? } else { echo "<br>"; } ?>
 *
 * Please do NOT remove the credits.
 *
 *****************************************************************************************/

$min_lvl=1000;
require('../../php_includes/cmaster.inc');

std_init();
if ($admin==0) {
	echo "Admin page only, sorry.";
	die;
}
$cTheme = get_theme_info();

std_theme_styles(1);
std_theme_body();

function cutOut( $what, $from = "" ) {
  $a_sq = split( "&", ( $from == "" ? str_replace( '&amp;', '&', $_SERVER[ "QUERY_STRING" ] ) : str_replace( '&amp;', '&', $from  ) ) );
  $sq = "";
  foreach( $a_sq as $a ) {
        if( $p = strpos( $a, $what ) === false ) { $sq .= ( $sq != "" ? '&amp;' : "" ).$a; }
  }
  return $sq;
}

if ($show=="yes") {

echo "<table border=1 width=100% cellspacing=0 cellpadding=2 border=0 bgcolor=#" . $cTheme->table_bgcolor . ">";
echo "<tr><td colspan=4><H2>Showing all registered channels...</H2></td></tr>";
 if ($cTheme->table_headimage!="") { $thi = " background=\"themes/data/" . $cTheme->sub_dir . "/" . $cTheme->table_headimage . "\""; }
  else { $thi = ""; }

$numpages = array( 50, 100, 200, 300, 500, 1000, 5000, 10000, 50000, 100000 );

  $numpg = ( isset( $_REQUEST[ "numpg" ] ) ? $_REQUEST[ "numpg" ] : $numpages[ 0 ] );
  if( !in_array( $numpg, $numpages ) ) { $numpg = $numpages[ 0 ]; }
  $all_entries = 0;
  $maxpages = 0;

  $pg = ( isset( $_REQUEST[ "pg" ] ) ? $_REQUEST[ "pg" ] : 0 );
  $q0 = "SELECT id FROM channels WHERE 1 = 1";
  $r0 = pg_safe_exec( $q0 );
  if( $r0 ) { $all_entries = pg_num_rows( $r0 ); }
  $maxpages = floor( $all_entries / $numpg );
  if( $pg * $numpg > $all_entries ) { $pg = $maxpages; }

  echo '<font face="arial,helvetica" size=-1>';
  echo '<form action="'.basename( $PHP_SELF ).'?'.cutOut( "pg=", cutOut( "numpg=" ) ).'" method="post">';
  echo '<tr>';
  echo '<td style="width:200px">&nbsp;';
  if( $pg > 0 ) {
    echo '<a href="'.basename( $PHP_SELF ).'?'.cutOut( "pg=", cutOut( "numpg=" ) ).'&amp;pg='.( $pg - 1 ).'&amp;numpg='.$numpg.'">';
  }
  echo 'Previous page';
  if( $pg > 0 ) {
    echo '</a>';
  }
  echo '</td>';
  echo '<td style="width:250px">&nbsp;';
  echo 'Channels per page: ';
  echo '<select size="1" name="numpg" id="select_numpg" onchange="this.form.submit();">';
  foreach( $numpages as $num ) {
    echo '<option value="'.$num.'"'.( $num == $numpg ? ' selected="selected"' : '' ).'>'.$num.'</option>';
  }
  echo '</select>';
  echo '</td>';
  echo '<td>&nbsp;';
  if( ( $pg + 1 ) * $numpg < ( $all_entries ) ) {
    echo '<a href="'.basename( $PHP_SELF ).'?'.cutOut( "pg=", cutOut( "numpg=" ) ).'&amp;pg='.( $pg + 1 ).'&amp;numpg='.$numpg.'">';
  }
  echo 'Next page';
  if( ( $pg + 1 ) * $numpg < ( $all_entries ) ) {
    echo '</a>';
  }
  echo '</td>';
  echo '<td>';
  echo '&nbsp;Jump to page: ';
  echo '<select name="pg" id="select_pg" onchange="this.form.submit();">';
  $pg_i = 0;
   while ( $pg_i <= $maxpages ) {
    echo '<option value="'.$pg_i.'"'.( $pg_i == $pg ? ' selected="selected"' : '' ).'>'.( $pg_i + 1 ).'</option>';
    $pg_i++;
  }
  echo '</select>';
  echo '</td>';
  echo '</tr>';
  echo '</form>';
  $page_stuff = '&amp;pg='.$pg.'&amp;numpg='.$numpg;

echo "<font face=\"arial,helvetica\" size=-1><tr" . $thi . " bgcolor=#" . $cTheme->table_headcolor . ">"
    ."<td><font face=\"arial,helvetica\" color=#" . $cTheme->table_headtextcolor . ">Id</font></td>"
    ."<td><font face=\"arial,helvetica\" color=#" . $cTheme->table_headtextcolor . ">Channel</font></td>"
    ."<td><font face=\"arial,helvetica\" color=#" . $cTheme->table_headtextcolor . ">Registered</font></td>"
    ."<td><font face=\"arial,helvetica\" color=#" . $cTheme->table_headtextcolor . ">Created</font></td></tr>";

$query = "SELECT id,name,registered_ts,channel_ts FROM channels WHERE deleted='0' AND flags!='0' ORDER BY id OFFSET ".( $pg * $numpg )." LIMIT ".( $numpg )."";
$result = pg_safe_exec($query);

while ( $row = pg_fetch_array( $result, NULL, PGSQL_ASSOC ) ) {
  echo "<tr" . $thi . " bgcolor=white>";
  echo '<td><font face="arial,helvetica" size=-1>' . $row[ "id" ] . '</font></td>';
  echo '<td><font face="arial,helvetica" size=-1>&nbsp;<a href="channels.php?id=' . $row[ "id" ] . '">' . $row[ "name" ] . '</a></font></td>';	
  echo '<td><font face="arial,helvetica" size=-1>&nbsp; ' . cs_time($row[ "registered_ts" ]) . '</font></td>';
  echo '<td><font face="arial,helvetica" size=-1>&nbsp; ' . cs_time($row[ "channel_ts" ]) . '</font></td>';
  echo "</tr>";
}
echo '</table>';
}
else { echo "<b>For 1000 Admins only!</b><br /><br />"
	   ."<a href=\"viewallchans.php?show=yes\">Show all registered channels!</a><br /><br />"
	   ."<b><font color=\"green\">NOTE: Using big 'Channels per page' value on a big network might take some time.</font></b>"
	   ."<br /><br />Watch out, you may find some secret channels that will break your party. Use this feature on your own risk! :P";
 }
?>
</body></html>

